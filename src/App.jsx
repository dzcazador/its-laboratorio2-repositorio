import { useState } from 'react'
import PortfolioPage from './pages/PortfolioPage';

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
     <PortfolioPage />
    </>
  )
}

export default App
