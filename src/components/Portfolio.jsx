import React from 'react';
import Grid from '@mui/material/Grid';
import ProjectCard from './ProjectCard';

const projects = [
  { title: 'RHPro', description: 'Desarrollo, Implementacion y Consultoria en RHPro', image: 'rhpro.jpg' },
  { title: 'PROFOOD', description: 'Viandas, Ultracongelados, Servicio de Catering', image: 'profood.png' },
  { title: 'TGYC', description: 'Tecnologia Aplicada y Consultoria Informatica', image: 'tgyc.png' },
  { title: 'GOOD', description: 'Alimentacion', image: 'good.png' },
  // Agrega más proyectos según sea necesario
];

const Portfolio = () => (
  <Grid container spacing={3}>
    {projects.map((project, index) => (
      <Grid item xs={12} sm={6} md={4} key={index}>
        <ProjectCard {...project} />
      </Grid>
    ))}
  </Grid>
);

export default Portfolio;
